import sbt._
import sbt.Keys._

object ProjectBuild extends Build {

  lazy val buildVersion = "0.1"

  lazy val root = Project(id = "Random Akka/Play/Scala experiments", base = file("."), settings = Project.defaultSettings).settings(
    organization := "mikefaille.test",
    description := "Scala Enumeration plugin for PlayFramework 2",
    version := buildVersion,
    scalaVersion := "2.10.0",
    resolvers += "Typesafe Releases" at "http://repo.typesafe.com/typesafe/releases/",
    resolvers += "Typesafe Snapshots" at "http://repo.typesafe.com/typesafe/snapshots/",
    libraryDependencies += "play" %% "play" % "2.1-RC2",
    libraryDependencies += "play" %% "play-test" % "2.1-RC2" % "test"

  )
}

