// Ref : 
// Bug to bypass with Play 2.1-RC2 https://play.lighthouseapp.com/projects/82401/tickets/932-new-json-api-and-companion-objects
// Main doc inspiration http://mandubian.com/2012/09/08/unveiling-play-2-dot-1-json-api-part1-jspath-reads-combinators/
import play.api.libs.json._
import play.api.libs.json.Reads._
import play.api.libs.functional.syntax._
import play.api.libs.json.util._

object Main extends App {

  // The case class

  import play.api.data.validation.ValidationError


  // defines a custom reads to be reused
  // a reads that verifies your value is not equal to a give value

  case class Person(name: String, age: Int)

  implicit val personReads = (
    (__ \ 'name).read[String] and
    (__ \ 'age).read[Int])(Person.apply _)

  val gizmojs = Json.obj(
    "name" -> "gremlins",
    "age" -> 10)
  val gizmo = gizmojs.validate[Person]
  println("Type is : " + gizmo.getClass().getName()) // Spoil : JsSuccess
  val gizmoObject = gizmo.get
  println("Type is : " + gizmoObject.getClass().getName()) // Spoil : Person

  //ErrorCase
  // Ramdom Input...
  val smart = Json.obj(
    "carName" -> "Smart",
    "performance" -> "bad")

  val smarties = smart.validate[Person]
  println("Type is : " + smarties.getClass().getName()) // Spoil : JsError --- maybe, with validator we can feedback easilly enduser about current issue
  val smartiesObject = smarties.get
  println("Type is : " + smartiesObject.getClass().getName()) // Spoil : It will simply Crash..
}